package com.open.cloud.common.lock;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 锁对象抽象
 * @author kong
 * @date 2021/07/08 22:42
 * blog: http://blog.kongyin.ltd
 */
@AllArgsConstructor
public class ZLock implements AutoCloseable {
    @Getter
    private final Object lock;

    private final DistributedLock locker;

    @Override
    public void close() throws Exception {

    }
}
