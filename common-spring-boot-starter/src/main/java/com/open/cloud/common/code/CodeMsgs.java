package com.open.cloud.common.code;

/**
 * 结果代码定义
 *
 * @author kong
 * @date 2021/2/13
 * blog: http://blog.kongyin.ltd
 */
public class CodeMsgs {

    /// =============================== 权限类型 ===============================
    /**
     * 认证失败（401000）
     */
    public static CodeMsg AUTH_FAIL = CodeMsg.typeBuilder().type(CodeMsgType.OAUTH_FAIL).suffixCode("000").message("身份验证失败").build();
    /**
     * 无访问权限（403000）
     */
    public static CodeMsg ACCESS_FAIL = CodeMsg.typeBuilder().type(CodeMsgType.ACCESS_FAIL).suffixCode("000").message("没有资源访问权限").build();

    /// =============================== 操作类型 ===============================
    /**
     * 成功（200000）
     */
    public static CodeMsg REQUEST_SUCCESS = CodeMsg.typeBuilder().type(CodeMsgType.SUCCESS).suffixCode("000").message("请求成功").build();
    /**
     * 操作成功（200100）
     */
    public static CodeMsg OPT_SUCCESS = CodeMsg.typeBuilder().type(CodeMsgType.SUCCESS).suffixCode("100").message("操作成功").build();
    /**
     * 创建成功（200101）
     */
    public static CodeMsg OPT_CREATE_SUCCESS = CodeMsg.typeBuilder().type(CodeMsgType.SUCCESS).suffixCode("101").message("创建成功").build();
    /**
     * 修改成功（200102）
     */
    public static CodeMsg OPT_UPDATE_SUCCESS = CodeMsg.typeBuilder().type(CodeMsgType.SUCCESS).suffixCode("102").message("修改成功").build();

    /// =============================== 服务全局错误 ===============================
    /**
     * 系统繁忙(-1)
     */
    public static CodeMsg SYS_BUSY = CodeMsg.builder().code("-1").message("系统繁忙").build();

    /**
     * 请求参数错误（400100）
     */
    public static CodeMsg REQUEST_PARAM_BAD = CodeMsg.typeBuilder().type(CodeMsgType.CLIENT_FAIL).suffixCode("100").message("请求参数错误").build();

    /**
     * 请求接口不存在（400100）
     */
    public static CodeMsg REQUEST_NOT_FOUND = CodeMsg.typeBuilder().type(CodeMsgType.FAIL).suffixCode("101").message("接口不存在").build();

    /// =============================== 业务异常 ===============================
    /**
     * 业务处理成功（402100）
     */
    public static CodeMsg SERVICE_BASE_SUCCESS = CodeMsg.typeBuilder().type(CodeMsgType.BIZ_FAIL).suffixCode("100").message("业务处理成功").build();
    /**
     * 业务处理错误（402500）
     */
    public static CodeMsg SERVICE_BASE_ERROR = CodeMsg.typeBuilder().type(CodeMsgType.BIZ_FAIL).suffixCode("500").message("业务处理失败").build();

    /// =============================== 系统错误 ===============================
    /**
     * 系统错误（500100）
     */
    public static CodeMsg SYSTEM_BASE_ERROR = CodeMsg.typeBuilder().type(CodeMsgType.FAIL).suffixCode("100").message("系统错误").build();


}
