package com.open.cloud.common.exception;

/**
 * 资源访问异常
 *
 * @author kong
 * @date 2021/2/12
 * blog: http://blog.kongyin.ltd
 */
public class AccessException extends AbstractBizException {
    public AccessException() {
    }

    public AccessException(String message) {
        super(message);
    }

    public AccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessException(Throwable cause) {
        super(cause);
    }

    public AccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
