package com.open.cloud.common.constant;

/**
 * 全局 公共常量
 * @author kong
 * @date 2021/07/07 22:03
 * blog: http://blog.kongyin.ltd
 */
public interface CommonConstant {

    /**
     * 文件分隔符
     */
    String PATH_SPLIT = "/";

    /**
     * 锁前缀
     */
    String LOCK_KEY_PREFIX = "LOCK_KEY";
}
