package com.open.cloud.common.context;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 上下文用户信息。
 * <p>在安全认证环境下，可以通过该类获取当前登录的用户信息</p>
 * @author kong
 * @date 2021/2/12
 * blog: http://blog.kongyin.ltd
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ContextUserDetails implements Serializable {

    /**
     * 用户ID
     */
    private String id;
    /**
     * 账号
     */
    private String code;
    /**
     * 密码
     */
    private String password;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 所属门店编码
     */
    private String storeCode;
    /**
     * 门店id
     */
    private String storeId;
    /**
     * 门店名称
     */
    private String storeName;
    /**
     * 邮箱地址
     */
    private String email;
    /**
     * 第三方id
     */
    private String openId;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别
     */
    private String sex;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 有效状态
     */
    private Boolean status;
    /**
     * 会员号
     */
    private String vipNo;
    /**
     * 角色信息
     */
    private List<String> roles;
    /**
     * 权限信息
     */
    private List<String> permissions;
}
