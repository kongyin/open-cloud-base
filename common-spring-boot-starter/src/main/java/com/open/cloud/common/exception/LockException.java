package com.open.cloud.common.exception;

/**
 * 分布式锁异常
 * @author kong
 * @date 2021/07/08 23:09
 * blog: http://blog.kongyin.ltd
 */
public class LockException extends RuntimeException {

    private static final long serialVersionUID = 6610083281801529147L;

    public LockException(String message) {
        super(message);
    }
}
