package com.open.cloud.common.exception;

/**
 * 身份认证异常
 *
 * @author kong
 * @date 2021/2/12
 * blog: http://blog.kongyin.ltd
 */
public class AuthenticateException extends AbstractBizException {
    public AuthenticateException() {
    }

    public AuthenticateException(String message) {
        super(message);
    }

    public AuthenticateException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticateException(Throwable cause) {
        super(cause);
    }

    public AuthenticateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
