package com.open.cloud.common.bean.db;

import java.util.Date;

/**
 * 数据库持久对象
 * @author kong
 * @date 2021/2/13
 * blog: http://blog.kongyin.ltd
 */
public abstract class AbstractPersistentObject implements IPersistentObject {

    /**
     * 获取持久化对象id
     *
     * @return 持久化对象id
     */
    public abstract String getId();

    /**
     * 获取备注
     *
     * @return 备注
     */
    public abstract String getRemark();

    /**
     * 获取创建人
     *
     * @return 创建人
     */
    public abstract String getCreateBy();

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public abstract Date getCreateTime();

    /**
     * 获取修改人
     *
     * @return 修改人
     */
    public abstract String getUpdateBy();

    /**
     * 获取修改时间
     *
     * @return 修改时间
     */
    public abstract Date getUpdateTime();

    /**
     * 获取最后修改时间戳
     *
     * @return 最后修改时间戳
     */
    public abstract Date getLastModify();
}
