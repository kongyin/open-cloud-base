Git 提交规范
# 1.Git使用指引(CLI)
提交格式：
```XML
<type> <no>: <subject>
// 
<body>
```
范例：
```
fix #251: add DataValidation
```
说明：
**type（必需）：用于说明 commit 的类别**
常用

- feat：新功能（feature）
- fix：修补

其他
-  br: 此项特别针对bug号，用于向测试反馈bug列表的bug修改情况
-  docs：文档（documentation）
-  style： 格式（不影响代码运行的变动）
-  refactor：重构（即不是新增功能，也不是修改bug的代码变动）
-  test：增加测试
-  chore：其他的小改动. 一般为仅仅一两行的改动, 或者连续几次提交的小改动属于这种
-  revert：feat(pencil): add 'graphiteWidth' option (撤销之前的commit)
-  upgrade：升级改造
-  optimize：优化
-  perf: Performance的缩写, 提升代码性能
-  test：新增测试用例或是更新现有测试
-  ci:主要目的是修改项目继续完成集成流程(例如Travis，Jenkins，GitLab CI,Circle)的提交
-  build: 主要目的是修改项目构建系统(例如glup，webpack，rollup的配置等)的提交

**no（必需）：任务号（例：jira任务号、hiit工单号）**
**subject（必需）：subject是 commit 目的的简短描述，不超过50个字符**
**body（可选）：部分是对本次 commit 的详细描述，可以分成多行**