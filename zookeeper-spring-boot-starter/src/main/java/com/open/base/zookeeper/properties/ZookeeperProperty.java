package com.open.base.zookeeper.properties;

import com.open.base.zookeeper.ZookeeperAutoConfiguration;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author kong
 * @date 2021/7/4 000417:08
 * blog: http://blog.kongyin.ltd
 */
@Setter
@Getter
@ConfigurationProperties(prefix = ZookeeperProperty.PREFIX)
public class ZookeeperProperty {
    public static final String PREFIX = "oc-frame.zk";
    /**
     * zk 链接集群 ，多个用逗号,隔开
     */
    private String connectString;

    /**
     * 会话超时时间
     */
    private int sessionTimeout = 15000;

    /**
     * 链接超时时间
     */
    private int connectionTimeout = 15000;

    /**
     *重试等待时间
     */
    private int baseSleepTime = 2000;

    /**
     * 重试最大次数
     */
    private int maxRetries = 10;
}
