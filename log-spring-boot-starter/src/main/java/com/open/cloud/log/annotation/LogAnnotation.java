package com.open.cloud.log.annotation;

import java.lang.annotation.*;

/**
 * 日志注解
 * @author kong
 * @date 2021/4/30
 * blog: http://blog.kongyin.ltd
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogAnnotation {

    /**
     * 模块
     */
    String module();

    /**
     * 记录执行参数
     */
    boolean recordRequestParam() default false;
}
