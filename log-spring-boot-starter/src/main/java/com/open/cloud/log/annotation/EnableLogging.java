package com.open.cloud.log.annotation;

import com.open.cloud.log.selector.LogImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动日志框架支持
 * 自动装配starter ，需要配置多数据源
 * @author kong
 * @date 2021/4/30
 * blog: http://blog.kongyin.ltd
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(LogImportSelector.class)
public @interface EnableLogging {
}
