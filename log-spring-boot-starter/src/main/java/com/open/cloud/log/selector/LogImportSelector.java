package com.open.cloud.log.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 自动装配
 * @author kong
 * @date 2021/4/30
 * blog: http://blog.kongyin.ltd
 */
public class LogImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{
                "com.open.cloud.log.aop.LogAnnotationAOP",
                "com.open.cloud.log.service.impl.LogServiceImpl",
                "com.open.cloud.log.config.LogAutoConfig"
        };
    }
}
