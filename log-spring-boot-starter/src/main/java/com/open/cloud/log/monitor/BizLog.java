package com.open.cloud.log.monitor;

import com.github.structlog4j.ILogger;
import com.github.structlog4j.SLoggerFactory;

/**
 * 业务结构化日志
 * @author kong
 * @date 2021/4/30
 * blog: http://blog.kongyin.ltd
 */
public class BizLog {

    private static final ILogger LOGGER =  SLoggerFactory.getLogger(BizLog.class);

    public static void info(String message,Object...params ) {
        LOGGER.info(message, params);
    }

    public static void debug(String message,Object...params ) {
        LOGGER.debug(message, params);
    }

    public static void trace(String message,Object...params ) {
        LOGGER.trace(message, params);
    }
}
