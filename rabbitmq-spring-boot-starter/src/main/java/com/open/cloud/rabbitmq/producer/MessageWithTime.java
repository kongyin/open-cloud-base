package com.open.cloud.rabbitmq.producer;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author kong
 * @date 2021/4/8
 * blog: http://blog.kongyin.ltd
 */
@Data
@AllArgsConstructor
public class MessageWithTime {
    private long id;
    private long time;
    private Object message;
}
