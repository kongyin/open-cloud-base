package com.open.cloud.rabbitmq.consumer;

import com.open.cloud.rabbitmq.common.DetailResponse;

/**
 * @author kong
 * @date 2021/4/16
 * blog: http://blog.kongyin.ltd
 */
public interface MessageConsumer {
    DetailResponse consume();
}
