package com.open.cloud.rabbitmq.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * @author kong
 * @date 2021/4/6
 * blog: http://blog.kongyin.ltd
 */
@Data
@ConfigurationProperties("oc-frame.rabbitmq")
public class RabbitMQProperties {
    /**
     * 是否启用
     */
    private boolean enable;
    /**
     * 地址
     */
    private String addresses;
    /**
     * 用户
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 虚拟主机
     */
    private String virtualHost;
    /**
     * 发布者确认
     */
    private boolean publisherConfirms = true;
}
