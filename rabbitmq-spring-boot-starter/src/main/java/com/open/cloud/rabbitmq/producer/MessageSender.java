package com.open.cloud.rabbitmq.producer;

import com.open.cloud.rabbitmq.common.DetailResponse;

/**
 * @author kong
 * @date 2021/4/8
 * blog: http://blog.kongyin.ltd
 */
public interface MessageSender {

    DetailResponse send(Object message);

    DetailResponse send(MessageWithTime messageWithTime);
}
