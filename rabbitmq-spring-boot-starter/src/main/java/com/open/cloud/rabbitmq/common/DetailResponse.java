package com.open.cloud.rabbitmq.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author kong
 * @date 2021/4/8
 * blog: http://blog.kongyin.ltd
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DetailResponse {
    /**
     * 是否成功
     */
    private boolean ifSuccess;
    /**
     * 失败编码
     */
    private String errorCode;
    /**
     * 失败信息
     */
    private String errMsg;
}
