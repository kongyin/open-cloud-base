package com.open.cloud.rabbitmq.cache;

import com.open.cloud.rabbitmq.common.DetailResponse;
import com.open.cloud.rabbitmq.common.FastRabbitMqConstants;
import com.open.cloud.rabbitmq.producer.MessageSender;
import com.open.cloud.rabbitmq.producer.MessageWithTime;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 重试缓存 retryCache的容器
 * @author kong
 * @date 2021/4/8
 * blog: http://blog.kongyin.ltd
 */
@Slf4j
public class RetryCache {
    private MessageSender sender;
    private boolean stop = false;
    private Map<Long, MessageWithTime> map = new ConcurrentSkipListMap<>();
    private AtomicLong id = new AtomicLong();

    public void setSender(MessageSender sender){
        this.sender = sender;
        startRetry();
    }

    private void startRetry() {
        new Thread(() -> {
            while (!stop){
                try {
                    Thread.sleep(FastRabbitMqConstants.RETRY_TIME_INTERVAL);
                }catch (InterruptedException e){
                    log.info("现场 sleep 失败",e);
                }
                long now = System.currentTimeMillis();

                for (Map.Entry<Long, MessageWithTime> entry : map.entrySet()) {
                    MessageWithTime messageWithTime = entry.getValue();

                    if(null != messageWithTime){
                        if (messageWithTime.getTime() + 3 * FastRabbitMqConstants.VALID_TIME < now) {
                            log.info("send message {} failed after 3 min ", messageWithTime);
                            del(entry.getKey());
                        } else if (messageWithTime.getTime() + FastRabbitMqConstants.VALID_TIME < now) {
                            DetailResponse res = sender.send(messageWithTime);

                            if (!res.isIfSuccess()) {
                                log.info("retry send message failed {} errMsg {}", messageWithTime, res.getErrMsg());
                            }
                        }
                    }
                }
            }
        }).start();
    }

    public long generateId() {
        return id.incrementAndGet();
    }

    public void add(MessageWithTime messageWithTime) {
        map.putIfAbsent(messageWithTime.getId(), messageWithTime);
    }

    public void del(long id) {
        map.remove(id);
    }

}
