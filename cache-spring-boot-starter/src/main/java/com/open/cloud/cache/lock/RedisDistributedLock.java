package com.open.cloud.cache.lock;

import com.open.cloud.common.lock.OldDistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.types.Expiration;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author kong
 * @date 2021/2/20 14:14
 **/
@Slf4j
public class RedisDistributedLock implements OldDistributedLock {

    /*
     * 通过lua脚本释放锁,来达到释放锁的原子操作
     */
    static {
        UNLOCK_LUA = "if redis.call(\"get\",KEYS[1]) == ARGV[1] " +
                "then " +
                "    return redis.call(\"del\",KEYS[1]) " +
                "else " +
                "    return 0 " +
                "end ";
    }

    private static final String UNLOCK_LUA;

    private final RedisTemplate<String,Object> redisTemplate;

    private final ThreadLocal<String> lockFlag = new ThreadLocal<>();

    public RedisDistributedLock(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public boolean lock(String key, long expire, int retryCount, long sleepMillis) {
        //设置锁
        boolean result = this.setLock(key,expire);
        //如果获取锁失败，按照传入的重试次数进行重试
        while (!result && retryCount --> 0){
            try {
                log.debug("get redisDistributeLock failed, retrying..." + retryCount);
                Thread.sleep(sleepMillis);
            } catch (InterruptedException e) {
                log.warn("Interrupted!", e);
                Thread.currentThread().interrupt();
            }
            result = setLock(key, expire);
        }
        return false;
    }

    /**
     * 设置锁
     * @param key
     * @param expire
     * @return
     */
    private boolean setLock(String key, long expire) {
        try {
            return redisTemplate.execute((RedisCallback<Boolean>) redisConnection -> {
                String uuid = UUID.randomUUID().toString();
                lockFlag.set(uuid);
                byte[] keyByte = redisTemplate.getStringSerializer().serialize(key);
                byte[] uuidByte = redisTemplate.getStringSerializer().serialize(uuid);
                return  (boolean) redisConnection.set(keyByte, uuidByte,
                        Expiration.from(expire, TimeUnit.MILLISECONDS), RedisStringCommands.SetOption.ifAbsent());
            });
        }catch (Exception e){
            log.error("设置redis锁发生异常", e);
        }
       return false;
    }

    @Override
    public boolean releaseLock(String key) {
        // 释放锁的时候，有可能因为持锁之后方法执行时间大于锁的有效期，此时有可能已经被另外一个线程持有锁，所以不能直接删除
        try {
            return redisTemplate.execute((RedisCallback<Boolean>) redisConnection -> {
                byte[] scriptByte = redisTemplate.getStringSerializer().serialize(UNLOCK_LUA);
                byte[] keyByte = redisTemplate.getStringSerializer().serialize(key);
                byte[] uuidByte = redisTemplate.getStringSerializer().serialize(lockFlag.get());
                return redisConnection.eval(scriptByte, ReturnType.BOOLEAN, 1, keyByte, uuidByte);
            });
        }catch (Exception e){
            log.error("释放redis锁发生异常", e);
        }finally {
            lockFlag.remove();
        }
        return false;
    }
}
