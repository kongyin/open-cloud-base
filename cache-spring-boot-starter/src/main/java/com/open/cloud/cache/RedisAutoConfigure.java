package com.open.cloud.cache;

import com.google.common.collect.Maps;
import com.open.cloud.cache.lock.RedisDistributedLock;
import com.open.cloud.cache.properties.CustomCacheProperties;
import com.open.cloud.cache.redis.RedisOps;
import com.open.cloud.cache.repository.CacheOps;
import com.open.cloud.cache.repository.CachePlusOps;
import com.open.cloud.cache.repository.Impl.RedisOpsImpl;
import com.open.cloud.cache.serializer.RedisObjectSerializer;
import com.open.cloud.common.lock.OldDistributedLock;
import com.open.cloud.common.utils.StrPool;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * redis 配置类
 *
 * @author kong
 * @date 2021/2/20 16:27
 **/
@ConditionalOnClass(RedisConnectionFactory.class)
@ConditionalOnProperty(prefix = CustomCacheProperties.PREFIX, name = "type", havingValue = "REDIS", matchIfMissing = true)
@EnableConfigurationProperties(CustomCacheProperties.class)
@RequiredArgsConstructor
public class RedisAutoConfigure {
    private final CustomCacheProperties cacheProperties;

    @Bean("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate();
        this.setSerializer(factory, redisTemplate);
        return redisTemplate;
    }

    @Bean("stringRedisTemplate")
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        this.setSerializer(factory, stringRedisTemplate);
        return stringRedisTemplate;
    }

    /**
     * 分布式锁
     *
     * @param redisTemplate 模板
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public OldDistributedLock distributedLock(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
        return new RedisDistributedLock(redisTemplate);
    }

    /**
     * 设置序列化器
     *
     * @param factory       连接工厂
     * @param redisTemplate 模板
     */
    private void setSerializer(RedisConnectionFactory factory, RedisTemplate redisTemplate) {
        RedisObjectSerializer redisObjectSerializer = new RedisObjectSerializer();
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.setValueSerializer(redisObjectSerializer);
        redisTemplate.setHashValueSerializer(redisObjectSerializer);
        redisTemplate.setConnectionFactory(factory);
    }

    @Bean
    @ConditionalOnMissingBean
    public RedisOps redisOps(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
        return new RedisOps(redisTemplate, cacheProperties.getCacheNullVal());
    }

    @Bean
    @ConditionalOnMissingBean
    public CacheOps cacheOps(RedisOps redisOps) {
        return new RedisOpsImpl(redisOps);
    }

    @Bean
    @ConditionalOnMissingBean
    public CachePlusOps cachePlusOps(RedisOps redisOps) {
        return new RedisOpsImpl(redisOps);
    }

    /**
     * 用于 @Cacheable 相关注解
     *
     * @param redisConnectionFactory 链接工厂
     * @return 缓存管理器
     */
    @Bean(name = "cacheManager")
    @Primary
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory){
        RedisCacheConfiguration redisCacheConfiguration = this.getDefConf();

        Map<String, CustomCacheProperties.Cache> configs = cacheProperties.getConfigs();
        Map<String, RedisCacheConfiguration> cacheConfigurations = Maps.newHashMap();

        Optional.ofNullable(configs).ifPresent(config ->{
            config.forEach((key,cache) -> {
                RedisCacheConfiguration cfg = handleRedisCacheConfiguration(cache, redisCacheConfiguration);
                cacheConfigurations.put(key, cfg);
            });
        });
        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults(redisCacheConfiguration)
                .withInitialCacheConfigurations(cacheConfigurations).build();

    }

    /**
     * 获取默认配置
     * @return
     */
    private RedisCacheConfiguration getDefConf() {
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .disableCachingNullValues()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new RedisObjectSerializer()));
        return this.handleRedisCacheConfiguration(cacheProperties.getDef(), redisCacheConfiguration);
    }

    /**
     * 处理redis缓存配置
     * @param redisProperties redis属性
     * @param config 配置
     * @return
     */
    private RedisCacheConfiguration handleRedisCacheConfiguration(CustomCacheProperties.Cache redisProperties, RedisCacheConfiguration config) {
        if (Objects.isNull(redisProperties)) {
            return config;
        }
        Duration timeToLive = redisProperties.getTimeToLive();
        String keyPrefix = redisProperties.getKeyPrefix();
        if (Objects.isNull(timeToLive)) {
            config = config.entryTtl(timeToLive);
        }
        if (Objects.isNull(keyPrefix)) {
            config = config.computePrefixWith(cacheName -> keyPrefix.concat(StrPool.COLON).concat(cacheName).concat(StrPool.COLON));
        } else {
            config = config.computePrefixWith(cacheName -> cacheName.concat(StrPool.COLON));
        }
        if (!redisProperties.isCacheNullValues()) {
            config = config.disableCachingNullValues();
        }
        if (!redisProperties.isUseKeyPrefix()) {
            config = config.disableKeyPrefix();
        }
        return config;
    }
}
