package com.open.cloud.cache.redis;

import java.io.Serializable;

/**
 * 空值
 * 解决缓存穿透
 * @author kong
 * @date 2021/2/20 16:58
 **/
public class NullVal implements Serializable {
    private static final long serialVersionUID = 1L;
}
