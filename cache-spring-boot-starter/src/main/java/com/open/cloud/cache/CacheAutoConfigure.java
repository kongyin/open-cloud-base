package com.open.cloud.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

/**
 *  缓存配置
 * @author kong
 * @date 2021/2/21 9:28
 **/
@Slf4j
@EnableCaching
@Import({RedisAutoConfigure.class})
public class CacheAutoConfigure {
}
