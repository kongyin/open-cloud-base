package com.open.cloud.cache.prefix;

/**
 * @author kong
 * @date 2021/2/13
 * blog: http://blog.kongyin.ltd
 */
public interface KeyPrefix {

    public int expireSeconds();

    public String getPrefix();
}
