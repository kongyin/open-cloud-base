package com.open.cloud.cache.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.Duration;

/**
 * 缓存封装key
 * @author kong
 * @date 2021/2/16
 * blog: http://blog.kongyin.ltd
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CacheKey {

    /**
     * redis key
     */
    @NonNull
    protected String key;
    /**
     * 超时时间 秒
     */
    @Nullable
    protected Duration expire;

    public CacheKey(String key) {
        this.key = key;
    }
}
