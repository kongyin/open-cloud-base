package com.open.cloud.cache.properties;

/**
 * 缓存类型
 * @author kong
 * @date 2021/2/20 16:15
 **/
public enum CacheType {
    /**
     * redis
     */
    REDIS;

    public boolean eq(CacheType cacheType) {
        return cacheType != null && this.name().equals(cacheType.name());
    }
}
