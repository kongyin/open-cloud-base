# open-cloud-base
> open-cloud-platform的基础开发包
## 介绍
微服务学习,努力打造开源项目

## 软件架构
软件架构说明 

## 说明
本着学习共享的态度，努力打造开源项目，希望能够帮助大家<br/>
因本人能力有限希望大家可以指出不足之处，我深表感谢

>本项目对开源形目进行学习，及优化改造 
>- **open-capacity-platform** 地址:https://gitee.com/owenwangwen/open-capacity-platform  
>- **lamp-cloud** 地址:https://gitee.com/zuihou111/lamp-cloud

## 反馈
* 欢迎提交`ISSUS`，请写清楚问题的具体原因，重现步骤和环境(上下文)
* 个人博客：[http://blog.kongyin.ltd/](http://blog.kongyin.ltd/)
* 个人邮箱：kongyin0921@163.com
* 个人公众号：[程序员Kong](https://gitee.com/kongyin/picture_bed/raw/master/wx_picture/20210211181215.png)
* 学习笔记：http://kongyin.gitee.io/learning-notes/
