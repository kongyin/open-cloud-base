package com.open.cloud.datasource.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author kong
 * @date 2021/2/12
 * blog: http://blog.kongyin.ltd
 */
@Data
@ConfigurationProperties(prefix = "oc-frame.mybatis-plus")
public class MybatisPlusProperties {
    /**
     * 是否开启自动填充字段
     */
    private Boolean enabled = true;
    /**
     * 是否开启了插入填充
     */
    private Boolean enableInsertFill = true;
    /**
     * 是否开启了更新填充
     */
    private Boolean enableUpdateFill = true;
    /**
     * 创建人字段名
     */
    private String createByField = "createBy";
    /**
     * 更新人字段名
     */
    private String updateByField = "updateBy";
    /**
     * 创建时间字段名
     */
    private String createTimeField = "createTime";
    /**
     * 更新时间字段名
     */
    private String updateTimeField = "updateTime";
    //----------分页-------------
    /**
     * 溢出总页数后是否进行处理
     */
    protected boolean overflow = false;
    /**
     * 单页限制 500 条，小于 0 如 -1 不受限制
     */
    protected long limit = 500L;
}
