package com.open.cloud.datasource.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.open.cloud.common.bean.db.AbstractPersistentObject;

import java.util.Date;

/**
 * mybatis plus 基础对象
 *
 * @author kong
 * @date 2021/2/12
 * blog: http://blog.kongyin.ltd
 */
public abstract class AbstractIBatisPo extends AbstractPersistentObject {

    /**
     * 持久化对象id
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    /**
     * 时间戳
     */
    private Date lastModify;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public String getCreateBy() {
        return this.createBy;
    }

    @Override
    public Date getCreateTime() {
        return this.createTime;
    }

    @Override
    public String getUpdateBy() {
        return this.updateBy;
    }

    @Override
    public Date getUpdateTime() {
        return this.updateTime;
    }

    @Override
    public Date getLastModify() {
        return this.lastModify;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setLastModify(Date lastModify) {
        this.lastModify = lastModify;
    }
}
